import json


class Gas:

    def __init__(self, species_data, input_data=dict, fraction_type="Y"):
        """Create a new instance of a gas phase object.

        :param species_data: Database with element data for relevant species.
        :param input_data: Fractions to initialize the gas phase
        :param fraction_type: Type of fraction, mass="Y", mole="X"

        :type species_data: dict
        :type input_data: json, dict
        :type fraction_type: str
        """
        self.species_data = species_data
        self._input_to_dict(input_data)
        if not self.input:
            raise ValueError("Empty mass fraction_type not allowed.")
        self._mass_or_mole(fraction_type)

    def molecular_weight(self):
        """Calculate the molecular weight for the gas phase.

        :return: Molecular weight in [g/mol]
        """
        if hasattr(self, "Y"):
            return self._mole_weight_from_mass()
        else:
            return self._mole_weight_from_mole()

    def _mole_weight_from_mole(self):
        """
        Calculate the molecular weight from X.

        :return: Molecular weight in [g/mol]
        """
        mole_weight = 0.0
        for species in self.X:
            mole_weight += self.species_data[species].molar_mass() \
                           * self.X[species]
        return mole_weight

    def _mole_weight_from_mass(self):
        """
        Calculate the molecular weight from Y.

        :return: Molecular weight in [g/mol]
        """
        mole_weight = 0.0
        for species in self.Y:
            mole_weight += self.Y[species] / self.species_data[species].molar_mass()
        return 1 / mole_weight

    def _mass_to_mole(self):
        """Convert mass fraction to mole fraction.

        :return: Mole fractions in X[-]
        :rtype: dict
        """
        mole_weight = self.molecular_weight()
        mole_fraction = {}
        for species in self.Y:
            mole_fraction[species] = self.Y[species] * mole_weight \
                                     / self.species_data[species].molar_mass()
        return mole_fraction

    def _mole_to_mass(self):
        """Convert mole fraction to mass fraction.

        :return: Mole fractions in Y[-]
        :rtype: dict
        """
        mole_weight = self.molecular_weight()
        mass_fraction = {}
        for species in self.X:
            mass_fraction[species] = self.X[species] \
                                     * self.species_data[species].molar_mass() \
                                     / mole_weight
        return mass_fraction

    def _mass_or_mole(self, fraction):
        """Set mass and mole fraction based on the fraction type

        :param fraction: "X" for mole fraction, "Y" for mass fraction [default]
        type fraction: str
        """
        if fraction == "Y":
            self.Y = self.input
            self.X = self._mass_to_mole()
        elif fraction == "X":
            self.X = self.input
            self.Y = self._mole_to_mass()
        else:
            raise ValueError("Chose either Y or X.")

    def _input_to_dict(self, input_data):
        """Convert the input date to a dict.

        :param input_data: Input from __init__
        :type input_data: str, dict
        :return: Input data
        :rtype: dict
        """
        if isinstance(input_data, str):
            inp = json.loads(input_data, encoding="utf-8")
        elif isinstance(input_data, dict):
            inp = input_data
        else:
            raise ValueError("Unsupported input type {}.".format(type(input_data)))
        self.input = inp

    def to_cantera(self, type='Y'):
        out = ""
        fraction = getattr(self, type)
        for sp in sorted(fraction):
            out = out + "{:s}:{:g},".format(sp, fraction[sp])
        return out[0:-1]
