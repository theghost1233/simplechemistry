from nose.tools import assert_equal, assert_almost_equal

from species import species


def test_full_elements():
    ch4 = species.Species(C=1, H=4, N=0, O=0)
    assert_equal(ch4.C, 1)
    assert_equal(ch4.H, 4)
    assert_equal(ch4.N, 0)
    assert_equal(ch4.O, 0)


def test_partial_elemets():
    ch4 = species.Species(C=1, H=4)
    assert_equal(ch4.C, 1)
    assert_equal(ch4.H, 4)
    assert_equal(ch4.N, 0)
    assert_equal(ch4.O, 0)


def test_empty_elements():
    empty = species.Species()
    assert_equal(empty.C, 0)
    assert_equal(empty.H, 0)
    assert_equal(empty.N, 0)
    assert_equal(empty.O, 0)


def test_molar_mass_CH4():
    ch4 = species.Species(C=1, H=4)
    assert_equal(ch4.molar_mass(), 16)


def test_molar_mass_C12():
    ch4 = species.Species(C=12, H=26)
    assert_equal(ch4.molar_mass(), 170)


def test_overwrite_elements():
    species.ELEMENTS = species._species(C=12.011, H=1.008, N=14.007, O=15.999)
    c2h6o = species.Species(2, 6, 0, 1)
    assert_almost_equal(c2h6o.molar_mass(), 46.07, 2)
