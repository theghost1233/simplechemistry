from nose.tools import assert_almost_equal, assert_equal, assert_raises

from gas.gas import Gas
from mixture.mixture import Mixture
from species.species import SPECIES_DATABASE

gas_1 = Gas(SPECIES_DATABASE, '{"O2": 0.232, "N2": 0.767}')
gas_2 = Gas(SPECIES_DATABASE, '{"CH4": 1}')


def test_mass_out_of_range_plus():
    assert_raises(ValueError, Mixture, gas_1, gas_2, {"mass": 14})


def test_mass_out_of_range_min():
    assert_raises(ValueError, Mixture, gas_1, gas_2, {"mass": -1})


def test_mix_mass_fraction():
    mix = Mixture(gas_1, gas_2, {"mass": 0.06})
    ref = {"O2": 0.21808, "N2": 0.72098, "CH4": 0.06}
    assert_equal(mix.mixture.Y, ref)


def test_mix_mole_fraction():
    mix = Mixture(gas_1, gas_2, {"mole": 0.06})
    ref = {"O2": 0.1969535, "N2": 0.74415436, "CH4": 0.06}
    assert_almost_equal(mix.mixture.X["O2"], ref["O2"], 2)
    assert_almost_equal(mix.mixture.X["N2"], ref["N2"], 2)


def test_mix_lambda():
    mix = Mixture(gas_1, gas_2, {"lambda": 0.9})
    ref = {"O2": 0.21814568, "N2": 0.72119715, "CH4": 0.06065717}
    assert_almost_equal(mix.mixture.Y['O2'], ref['O2'], 2)
    assert_almost_equal(mix.mixture.Y['N2'], ref['N2'], 2)
    assert_almost_equal(mix.mixture.Y['CH4'], ref['CH4'], 2)


def test_mix_phi():
    mix = Mixture(gas_1, gas_2, {"phi": 1 / 0.9})
    ref = {"O2": 0.21814568, "N2": 0.72119715, "CH4": 0.06065717}
    assert_almost_equal(mix.mixture.Y['O2'], ref['O2'], 2)
    assert_almost_equal(mix.mixture.Y['N2'], ref['N2'], 2)
    assert_almost_equal(mix.mixture.Y['CH4'], ref['CH4'], 2)


def test_mix_lambda_and_phi():
    mix = Mixture(gas_1, gas_2, {"phi": 0.5})
    ref = Mixture(gas_1, gas_2, {"lambda": 2})
    assert_almost_equal(mix.mixture.Y['O2'], ref.mixture.Y['O2'], 4)
    assert_almost_equal(mix.mixture.Y['N2'], ref.mixture.Y['N2'], 4)
    assert_almost_equal(mix.mixture.Y['CH4'], ref.mixture.Y['CH4'], 4)


def test_stoichiometric_mixture_fraction():
    mix = Mixture(gas_1, gas_2, {"mass": 1})
    ref = 0.055
    assert_almost_equal(mix.stoichiometric_mixture_fraction(), ref, 3)


def test_stoichiometric_mixture_fraction_combined():
    gas_3 = Gas(SPECIES_DATABASE, '{"C12H26": 1}')
    gas_4 = Gas(SPECIES_DATABASE, '{"C12H26": 0.75, "CH4": 0.25}')
    mix_ch4 = Mixture(gas_1, gas_2, {"mass": 1})
    mix_c12 = Mixture(gas_1, gas_3, {"mass": 1})
    z1 = 0.25 * mix_ch4.stoichiometric_mixture_fraction() + 0.75 * mix_c12.stoichiometric_mixture_fraction()
    z2 = Mixture(gas_1, gas_4, {"mass": 1}).stoichiometric_mixture_fraction()
    assert_almost_equal(z1, z2, 3)


def test_stoichiometric_air_fuel_ratio():
    mix = Mixture(gas_1, gas_2, {"mass": 1})
    ref = 17.224
    assert_almost_equal(mix.stoichiometric_air_fuel_ratio(), ref, 3)


def test_stoichiometric_fuel_air_ratio():
    mix = Mixture(gas_1, gas_2, {"mass": 1})
    ref = 0.058
    assert_almost_equal(mix.stoichiometric_fuel_air_ratio(), ref, 3)


def test_stoichiometric_equation_ch4():
    mix = Mixture(gas_1, gas_2, {'mass': 1})
    ref = '1.0 CH4+2.0 O2<==>1.0 CO2+2.0 H2O'
    assert_equal(mix.stoichiometric_equation(), ref)
