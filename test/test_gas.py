from nose.tools import assert_equal, assert_almost_equal

from gas.gas import Gas
from species.species import SPECIES_DATABASE

inp = '{"O2": 0.232, "N2": 0.767}'
ref = {"O2": 0.232, "N2": 0.767}


def test_input_string_to_dict():
    gas = Gas(SPECIES_DATABASE, inp)
    assert_equal(gas.input, ref)


def test_input_to_mass_fraction():
    gas = Gas(SPECIES_DATABASE, inp)
    assert_equal(gas.Y, ref)


def test_input_to_mole_fraction():
    gas = Gas(SPECIES_DATABASE, '{"O2": 0.2095, "N2":0.79165}', "X")
    assert_almost_equal(gas.Y["O2"], ref["O2"], 2)
    assert_almost_equal(gas.Y["N2"], ref["N2"], 2)


def test_input_dict():
    gas = Gas(SPECIES_DATABASE, ref)
    assert_equal(gas.Y, ref)


def test_molecular_weight():
    gas = Gas(SPECIES_DATABASE, inp)
    assert_almost_equal(gas.molecular_weight(), 28.9, 1)


def test_mass_to_mole():
    gas = Gas(SPECIES_DATABASE, inp)
    assert_almost_equal(gas.X["O2"], 0.21, 2)
    assert_almost_equal(gas.X["N2"], 0.79, 2)


def test_mass_to_mole_to_mass():
    gas = Gas(SPECIES_DATABASE, inp)
    assert_equal(gas._mole_to_mass(), ref)


def test_to_cantera():
    gas = Gas(SPECIES_DATABASE, inp)
    assert_equal(gas.to_cantera(), "N2:0.767,O2:0.232")
