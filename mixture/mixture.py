from collections import defaultdict

from gas.gas import Gas
from species.species import Species


class Mixture:
    def __init__(self, gas_1, gas_2, mixing_type={"mass": 1}):
        """Create a new mixture by mixing two gas phases.

        :param gas_1: The main gas component. (Oxidizer)
        :param gas_2: The secondary gas component, this will be added to gas one
        according to the mixing_type. (Fuel)
        :param mixing_type: How to mix and in which proportion.
        :type gas_1: Gas
        :type gas_2: Gas
        :type mixing_type: dict
        """
        self.gas_1 = gas_1
        self.gas_2 = gas_2
        self.mixing_type = mixing_type
        if "mass" in mixing_type or "mole" in mixing_type:
            self.mixture = self.mix_mass_or_mole()
        elif 'lambda' in mixing_type or 'phi' in mixing_type:
            self.mix_lambda_or_phi(self.mixing_type)
        else:
            ValueError("Unsupported mixing type")

    def mix_mass_or_mole(self):
        """Mix the two gases based on the fraction type and amount.

        :return: New instance of a Gas mixture
        :rtype: Gas
        """
        if "mass" in self.mixing_type:
            fraction = self.mixing_type["mass"]
            fraction_string = "Y"
        elif "mole" in self.mixing_type:
            fraction = self.mixing_type["mole"]
            fraction_string = "X"
        else:
            raise ValueError("{} is not supported.".format(self.mixing_type.keys()))
        if 0 <= fraction <= 1:
            new_mixture = defaultdict(float)
            for species in self.gas_1.Y:
                new_mixture[species] += self.gas_1.__getattribute__(fraction_string)[species] * (1 - fraction)
            for species in self.gas_2.Y:
                new_mixture[species] += self.gas_2.__getattribute__(fraction_string)[species] * fraction
            return Gas(self.gas_1.species_data, new_mixture, fraction_string)
        else:
            raise ValueError("Mixing_type needs to be between 0 and 1.")

    def mix_lambda_or_phi(self, mixing_type):
        """Convert lambda or phi to mass based mixing.

        :param mixing_type: How to mix and in which proportion.
        :type mixing_type: dict
        """
        if 'lambda' in mixing_type:
            afr = self.stoichiometric_air_fuel_ratio() * mixing_type['lambda']
        elif 'phi' in mixing_type:
            afr = self.stoichiometric_air_fuel_ratio() * (1 / mixing_type['phi'])
        else:
            raise ValueError
        z = 1 / (1 + afr)
        self.mixing_type = {'mass': z}
        self.mixture = self.mix_mass_or_mole()

    def stoichiometric_mixture_fraction(self):
        """Calculate the stoichiometric mixture fraction.

        :return: stoichiometric mixture fraction
        :rtype: float
        """
        mole_o2_needed = self._stoichiometric_o2()
        oxidizer_mass = self._oxidizer_mass(mole_o2_needed)
        return self.gas_2.molecular_weight() / (self.gas_2.molecular_weight() + oxidizer_mass)

    def stoichiometric_air_fuel_ratio(self):
        """Calculate the stoichiometric air fuel ratio.

        :return: stoichiometric air fuel ratio
        :rtype: float
        """
        return 1 / self.stoichiometric_mixture_fraction() - 1

    def stoichiometric_fuel_air_ratio(self):
        """Calculate the stoichiometric fuel air ratio.

        :return: stoichiometric fuel air ratio
        :rtype: float
        """
        return 1 / self.stoichiometric_air_fuel_ratio()

    def stoichiometric_equation(self):
        """Calculate the stoichiometric coefficient's

        Calculates the stoichiometric coefficient's under the assumption
        that all o2 is used for combustion and the relevant products are
        CO2 and H2O.

        :return: String with a balanced reaction equation
        :rtype: str
        """
        n_o2 = self._stoichiometric_o2()
        n_co2 = sum([self.gas_2.species_data[i].C * self.gas_2.X[i] for i in self.gas_2.X])
        n_h2o = sum([self.gas_2.species_data[i].H * self.gas_2.X[i] for i in self.gas_2.X]) / 2

        fuel_string = ''
        for sp in self.gas_2.X:
            fuel_string = fuel_string + '{} {}+'.format(self.gas_2.X[sp], sp)
        o2_string = '{} O2<==>'.format(n_o2)
        product_string = '{} CO2+{} H2O'.format(n_co2, n_h2o)
        return fuel_string + o2_string + product_string

    def _oxidizer_mass(self, mole_o2_needed):
        """Calculate the oxidizer mass.

        Calculate the oxidizer mass based on the amount of O2 which is need for complete combustion
        and the individual components of the oxidizer.

        :param mole_o2_needed: The amount of O2 in mole which is needed for complete combustion
        :return: Mass of the oxidizer for complete combustion.
        :rtype: float
        """
        oxidizer_mass = 0.0
        for species in self.gas_1.X:
            multiplier = self.gas_1.X[species] / self.gas_1.X["O2"]
            oxidizer_mass += mole_o2_needed * multiplier * self.gas_1.species_data[species].molar_mass()
        return oxidizer_mass

    def _stoichiometric_o2(self):
        """Calculate stoichiometric O2.

        Assume complete combustion where:
            C needs 1 O2
            H needs 1/4 O2
            N needs 0 O2
            O needs -1/2 O2

        :return: mole O2 needed for complete combustion
        :rtype: float
        """
        fuel_element_names = Species()._fields
        fuel_elements = defaultdict(float)
        stoichiometric_factors = Species(1, 1 / 4, 0, -1 / 2)
        mole_o2_needed = 0.0
        for element in fuel_element_names:
            for species in self.gas_2.X:
                fuel_elements[element] += self.gas_2.species_data[species].__getattribute__(element) \
                                          * self.gas_2.X[species]
            mole_o2_needed += fuel_elements[element] * stoichiometric_factors.__getattribute__(element)
        return mole_o2_needed
