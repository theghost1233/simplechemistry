from collections import namedtuple

_species = namedtuple("_species", "C H N O")
ELEMENTS = _species(C=12, H=1, N=14, O=16)


class Species(_species):

    def __new__(cls, C=0, H=0, N=0, O=0):
        return super(Species, cls).__new__(cls, C, H, N, O)

    def molar_mass(self):
        """Calculates molar mass based on elements.

        :return: molar mass in [g/mol]
        """
        mol_mass = 0
        for i in ELEMENTS._fields:
            mol_mass += self.__getattribute__(i) * ELEMENTS.__getattribute__(i)
        return mol_mass


SPECIES_DATABASE = {
    "O2": Species(0, 0, 0, 2),
    "N2": Species(0, 0, 2, 0),
    "CO2": Species(1, 0, 0, 2),
    "H2O": Species(0, 2, 0, 1),
    "CH4": Species(1, 4, 0, 0),
    "C12H26": Species(12, 26, 0, 0)
}
